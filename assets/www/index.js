    var pictureSource;   // picture source
    var destinationType; // sets the format of returned value 
    var pictureCount = 0; // number of pictures taken
    var server = "http://pictagram.herokuapp.com"
    //var server = "http://192.168.0.20:3000";
    var store;
    
    // Wait for PhoneGap to connect with the device
    document.addEventListener("deviceready",onDeviceReady,false);
    
    function getPhoneInfo() {
        var element = document.getElementById('deviceProperties');

        element.innerHTML = 'Device Name: '     + device.name     + '<br />' + 
                            'Device PhoneGap: ' + device.phonegap + '<br />' + 
                            'Device Platform: ' + device.platform + '<br />' + 
                            'Device UUID: '     + device.uuid     + '<br />' + 
                            'Device Version: '  + device.version  + '<br />';
    }

    // PhoneGap is ready to be used!
    function onDeviceReady() {
        store = window.localStorage;
        store.clear(); //TODO: Remove in production
        pictureSource=navigator.camera.PictureSourceType;
        destinationType=navigator.camera.DestinationType;
        getPhoneInfo();
        if (store["pictagramCount"]==null) store["pictagramCount"] = 0;
        if (store["picturesCount"]==null) store["picturesCount"] = 0; 
        //$.mobile.changePage("#newpictagram");
        status('program loaded');
        //uploadPictures();
    }
    
    // Add row to upload queue, we have a queue to make sure the pictagram gets
    // uploaded later if we dont have internet connection right now.
    function addToPictagramQueue()
    {
    	pictagram = new Object();
    	id = parseInt(store["pictagramCount"]) + 1;
    	
    	pictagram.title = $("#pictagramtitle").val();
    	pictagram.picture1 = $("#image1").attr("src");
    	pictagram.picture2 = $("#image2").attr("src");
    	pictagram.picture3 = $("#image3").attr("src");
    	pictagram.picture4 = $("#image4").attr("src");
    	pictagram.uuid = device.uuid;
    	pictagram.count = pictureCount;

    	store["pictagramQueue"+id]=JSON.stringify(pictagram);
    	store["pictagramCount"]=id;
    	status('adding pictagram to upload queue');
    }
    
    // This gets called when the send pictagram button is pressed
    function sendPictagram()
    {
    	if (pictureCount == 0) {alert("please add a picture"); return} 

    	//Send to server
    	status("send pictagram ...");
    	addToPictagramQueue();
    	uploadPictagrams();
    	
    	//ResetForm
    	cancelPictagram();
    	
    	//TODO: this does not serve the offline mode purpose anymore, redo the db
    	store["pictagramCount"] = 0;
    	store["picturesCount"] = 0;
    	
    }
    
    function cancelPictagram()
    {
    	pictureCount = 0;
    	$("#image1").attr("src",'');
    	$("#image2").attr("src",'');
    	$("#image3").attr("src",'');
    	$("#image4").attr("src",'');
    	$("#image1").hide();
    	$("#image2").hide();
    	$("#image3").hide();
    	$("#image4").hide();
    	$.mobile.changePage("#home");
    }
    
    // Take pictagrams from store, create them on server and queue
    // their pictures for uploading
    function uploadPictagrams()
    {
    	var count = store["pictagramCount"];
    	var pictagram_id;
    	for(i=1;i<=count;i++)
    		{
    			if (store["pictagramQueue"+i] != null) {
    				status("uploading pictagram"+i);
    				var params = JSON.parse(store["pictagramQueue"+i]);
    				var url = server + "/pictagrams.json";
    				

    				$.post(url, params, 
    						function (data) {
    							pictagram_id = JSON.stringify(data);
    							status('new pictagram id:'+pictagram_id);
    							addToPictureQueue(pictagram_id,params.picture1);
    							addToPictureQueue(pictagram_id,params.picture2);
    							addToPictureQueue(pictagram_id,params.picture3);
    							addToPictureQueue(pictagram_id,params.picture4);
    							removeFromStore("pictagramQueue"+i);
    						});
    			  
    			}
    		}
    }
    
    // Add file to picture upload queue, file is the picture
    // id is the pictagram id on server
    function addToPictureQueue(id, file)
    {
    	status('add to picture queue '+ 'id: ' + id + 'file: '+ file);
    	if (file != '')
    		{
    		status(file + ' added to upload queue for pictagram id ' + id);
	    	var picture = new Object();
	    	
	    	picture.id = id;
	    	picture.file = file;
	    	
	    	var count = parseInt(store["picturesCount"]) + 1;
	    	store["picture"+count] = JSON.stringify(picture);
	    	store["picturesCount"] = count;
	    	uploadPictures();
    		}
    }
    
    // upload pictures listed in the picture queue
    function uploadPictures()
    {
    	status('uploadPictures()');
    	var count = parseInt(store["picturesCount"]);
    	status('upload pictures count:' + count);
    	for(i=1;i<=count;i++)
    		{
    			key= "picture"+i;
    			if (store[key] != null) uploadPhoto(key);
    		}
    }
    
    // upload a single picture to the server
    function uploadPhoto(key) {
    	var picture = JSON.parse(store[key]);
    	imageURI = picture.file;
    	status('uploading image: ' + imageURI + ' id: ' + picture.id);
        var options = new FileUploadOptions();
        options.fileKey="file";
        options.fileName=imageURI.substr(imageURI.lastIndexOf('/')+1);
        options.mimeType="image/jpeg";

        var params = new Object();
        params.pictagram_id = parseInt(picture.id);

        options.params = params;
        var ft = new FileTransfer();
        ft.upload(imageURI, server + "/pictures.json", removeFromStore(key), fail, options);
    }
    
    function removeFromStore(key)
    {
    	store.removeItem(key); 
    }

    function win(r) {
        console.log("Code = " + r.responseCode);
        console.log("Response = " + r.response);
        console.log("Sent = " + r.bytesSent);
    }

    function fail(error) {
        alert("An error has occurred: Code = " = error.code);
    }

    // display status
    function status(s) {
    	$(".status").html(s);
    	console.log('ABC:' + s);
    }
    
    // Called when a photo is successfully retrieved
    function onPhotoURISuccess(imageURI) {
      status("took a photo");
      pictureCount++;
      if (pictureCount>3) $("#addpicture").hide();
      if (pictureCount==1) $("#sendpictagram").show();
      var image = document.getElementById('image'+pictureCount);

      // Unhide image elements
      image.style.display = 'block';

      // Show the captured photo
      // The inline CSS rules are used to resize the image
      image.src = imageURI;
    }

    // A button will call this function
    //
    function capturePhoto() {
      // Take picture using device camera and retrieve image as base64-encoded string
      navigator.camera.getPicture(onPhotoURISuccess, onFail, { quality: 50, destinationType: destinationType.FILE_URI });
    }

    // Called if something bad happens.
    // 
    function onFail(message) {
      alert('Failed because: ' + message);
    }
